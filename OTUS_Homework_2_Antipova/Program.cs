﻿using Microsoft.Extensions.DependencyInjection;
using OTUS_Homework_2_Antipova.Core.Abstractions;
using OTUS_Homework_2_Antipova.Core.Services.Commands;

namespace OTUS_Homework_2_Antipova.Presentation;

public class Program
{
    public static async Task Main(string[] args)
    {
        IServiceCollection services = new ServiceCollection();

        Startup startup = new Startup();
        startup.ConfigureServices(services);

        IServiceProvider serviceProvider = services.BuildServiceProvider();

        await GetUserCommand(serviceProvider.GetService<IUnitOfWork>()!);

    }

    public static async Task GetUserCommand(IUnitOfWork unitOfWork) 
    {
        Invoker invoker = new Invoker(unitOfWork);

        var startCommand = invoker.GetStartCommand();
        Console.WriteLine(await startCommand.Execute());

        while (true)
        {
            var input = Console.ReadLine();
            try
            {
                var command = invoker.GetCommand(input!);
                Console.WriteLine(await command.Execute());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                
            }
            Console.WriteLine(await startCommand.Execute());
        }
    }
}