﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OTUS_Homework_2_Antipova.Core.Abstractions;
using OTUS_Homework_2_Antipova.Core.Models;
using OTUS_Homework_2_Antipova.Core.Services.Repositories;
using OTUS_Homework_2_Antipova.Data;
using OTUS_Homework_2_Antipova.Data.Abstractions;
using OTUS_Homework_2_Antipova.Data.Models;
using OTUS_Homework_2_Antipova.Data.Providers;

namespace OTUS_Homework_2_Antipova.Presentation;
public class Startup
{
    IConfigurationRoot Configuration { get; }

    public Startup()
    {
        var builder = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json");

        Configuration = builder.Build();
    }

    public void ConfigureServices(IServiceCollection services)
    {
       // services.AddLogging();
        services.AddSingleton<IConfigurationRoot>(Configuration);

        services.AddSingleton<DbContext, GoodsMagagementDbContext>();
        services.AddDbContext<GoodsMagagementDbContext>(optionsBuilder => 
            optionsBuilder.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"))
        );

        
        services.AddSingleton<IRepository<Goods>, GoodsRepository>();
        services.AddSingleton<IRepository<GoodsCategory>, GoodsCategoryRepository>();
        services.AddSingleton<IRepository<GoodsStock>, GoodsStockRepository>();
        services.AddSingleton<IRepository<Stock>, StockRepository>();

        services.AddSingleton<IProvider<GoodsDbo>, GoodsProvider>();
        services.AddSingleton<IProvider<GoodsCategoryDbo>, GoodsCategoryProvider>();
        services.AddSingleton<IProvider<GoodsStockDbo>, GoodsStockProvider>();
        services.AddSingleton<IProvider<StockDbo>, StockProvider>();

        services.AddSingleton<IUnitOfWork, UnitOfWork>();
    }
}