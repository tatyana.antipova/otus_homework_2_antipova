﻿using Microsoft.EntityFrameworkCore;
using OTUS_Homework_2_Antipova.Data.Abstractions;
using OTUS_Homework_2_Antipova.Data.Models;

namespace OTUS_Homework_2_Antipova.Data.Providers;

public class StockProvider : IProvider<StockDbo>
{
    private readonly GoodsMagagementDbContext _dbContext;

    public StockProvider(GoodsMagagementDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<IEnumerable<StockDbo>> GetAllAsync()
    {
        var result = await _dbContext.Stock
            .AsNoTracking()
            .ToListAsync();
        return result;
    }

    public async Task AddAsync(StockDbo item)
    {
        await _dbContext.Stock.AddAsync(item);
        _dbContext.SaveChanges();
    }        
}
