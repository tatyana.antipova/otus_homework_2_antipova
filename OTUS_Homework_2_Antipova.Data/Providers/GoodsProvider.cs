﻿using Microsoft.EntityFrameworkCore;
using OTUS_Homework_2_Antipova.Data.Abstractions;
using OTUS_Homework_2_Antipova.Data.Models;

namespace OTUS_Homework_2_Antipova.Data.Providers;

public class GoodsProvider : IProvider<GoodsDbo>
{
    private readonly GoodsMagagementDbContext _dbContext;

    public GoodsProvider(GoodsMagagementDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<IEnumerable<GoodsDbo>> GetAllAsync()
    {
        var result = await _dbContext.Goods
            .AsNoTracking()
            .ToListAsync();
        return result;
    }

    public async Task AddAsync(GoodsDbo item)
    {
        var request = _dbContext.Goods.AddAsync(item);

        _dbContext.SaveChanges();
    }        
}
