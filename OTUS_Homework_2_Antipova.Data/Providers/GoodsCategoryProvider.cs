﻿using Microsoft.EntityFrameworkCore;
using OTUS_Homework_2_Antipova.Data.Abstractions;
using OTUS_Homework_2_Antipova.Data.Models;

namespace OTUS_Homework_2_Antipova.Data.Providers;

public class GoodsCategoryProvider : IProvider<GoodsCategoryDbo>
{
    private readonly GoodsMagagementDbContext _dbContext;

    public GoodsCategoryProvider(GoodsMagagementDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<IEnumerable<GoodsCategoryDbo>> GetAllAsync()
    {
        var result = await _dbContext.GoodsCategory
            .AsNoTracking()
            .ToListAsync();
        return result;
    }

    public async Task AddAsync(GoodsCategoryDbo item)
    {
        await _dbContext.GoodsCategory.AddAsync(item);
        _dbContext.SaveChanges();
    }        
}
