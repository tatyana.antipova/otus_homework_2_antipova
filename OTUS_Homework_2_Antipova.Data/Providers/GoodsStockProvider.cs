﻿using Microsoft.EntityFrameworkCore;
using OTUS_Homework_2_Antipova.Data.Abstractions;
using OTUS_Homework_2_Antipova.Data.Models;

namespace OTUS_Homework_2_Antipova.Data.Providers;

public class GoodsStockProvider : IProvider<GoodsStockDbo>
{
    private readonly GoodsMagagementDbContext _dbContext;

    public GoodsStockProvider(GoodsMagagementDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<IEnumerable<GoodsStockDbo>> GetAllAsync()
    {
        var result = await _dbContext.GoodsStock
            .AsNoTracking()
            .ToListAsync();
        return result;
    }

    public async Task AddAsync(GoodsStockDbo item)
    {
        await _dbContext.GoodsStock.AddAsync(item);
        _dbContext.SaveChanges();
    }        
}
