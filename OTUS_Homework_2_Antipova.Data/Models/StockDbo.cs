﻿namespace OTUS_Homework_2_Antipova.Data.Models;

public class StockDbo
{
    public int Id { get; set; }

    public string Title { get; set; }

    public int Capacity { get; set; }

    public string? Address { get; set; }

    public decimal? AddressLatitude { get; set; }

    public decimal? AddressLongitude { get; set; }

    public virtual ICollection<GoodsStockDbo> GoodsStock { get; set; }

    public virtual ICollection<GoodsDbo> Goods { get; set; }
}
