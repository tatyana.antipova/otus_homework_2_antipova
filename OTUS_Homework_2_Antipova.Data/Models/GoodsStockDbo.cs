﻿namespace OTUS_Homework_2_Antipova.Data.Models;

public class GoodsStockDbo
{
    public int Id { get; set; }

    public int GoodsId { get; set; }
    public GoodsDbo Goods { get; set; }

    public int StockId { get; set; }
    public StockDbo Stock { get; set; }

    public int Count { get; set; }
}
