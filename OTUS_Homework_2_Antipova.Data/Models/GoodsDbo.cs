﻿namespace OTUS_Homework_2_Antipova.Data.Models;

public class GoodsDbo
{
    public int Id { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }

    public decimal BasePrice { get; set; }

    public int? CategoryId { get; set; }
    public virtual GoodsCategoryDbo? Category { get; set; }

    public int? GoodsStockId { get; set; }
    public virtual ICollection<GoodsStockDbo> GoodsStock { get; set; }

    public int? StockId { get; set; }
    public virtual ICollection<StockDbo> Stock { get; set; }
}
