﻿namespace OTUS_Homework_2_Antipova.Data.Models;

public class GoodsCategoryDbo
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public virtual ICollection<GoodsDbo> Goods { get; set; }
}
