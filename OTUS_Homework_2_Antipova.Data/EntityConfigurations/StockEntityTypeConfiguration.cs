﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OTUS_Homework_2_Antipova.Data.Models;

namespace OTUS_Homework_2_Antipova.Data.EntityConfigurations;

internal class StockEntityTypeConfiguration : IEntityTypeConfiguration<StockDbo>
{
    public void Configure(EntityTypeBuilder<StockDbo> builder)
    {
        builder.ToTable("stock");

        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .ValueGeneratedOnAdd()
            .IsRequired();

        builder.Property(x => x.Title)
            .HasMaxLength(200)
            .IsRequired();

        builder.HasMany(e => e.GoodsStock)
            .WithOne(e => e.Stock)
            .IsRequired(false)
            .HasForeignKey(x => x.StockId);
    }
}
