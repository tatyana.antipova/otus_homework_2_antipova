﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OTUS_Homework_2_Antipova.Data.Models;


namespace OTUS_Homework_2_Antipova.Data.EntityConfigurations;

internal class GoodsEntityTypeConfiguration : IEntityTypeConfiguration<GoodsDbo>
{
    public void Configure(EntityTypeBuilder<GoodsDbo> builder)
    {
        builder.ToTable("goods");

        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .ValueGeneratedOnAdd()
            .IsRequired();

        builder.Property(x => x.Title)
            .HasMaxLength(200)
            .IsRequired();

        builder.Property(x => x.BasePrice)
            .IsRequired();


        builder.HasOne(x => x.Category)
            .WithMany(g => g.Goods)
            .IsRequired(false)
            .OnDelete(DeleteBehavior.NoAction);

        builder.Navigation(x => x.Category)
            .IsRequired(false);
    }
}
