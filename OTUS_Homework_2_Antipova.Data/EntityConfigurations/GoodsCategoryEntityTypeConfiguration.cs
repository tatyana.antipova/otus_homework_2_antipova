﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OTUS_Homework_2_Antipova.Data.Models;

namespace OTUS_Homework_2_Antipova.Data.EntityConfigurations;

internal class GoodsCategoryEntityTypeConfiguration : IEntityTypeConfiguration<GoodsCategoryDbo>
{
    public void Configure(EntityTypeBuilder<GoodsCategoryDbo> builder)
    {
        builder.ToTable("goods_gategory");

        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .ValueGeneratedOnAdd()
            .IsRequired();

        builder.Property(x => x.Name)
            .HasMaxLength(200)
            .IsRequired();

        builder.HasMany(x => x.Goods)
            .WithOne(x => x.Category);

    }
}
