﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OTUS_Homework_2_Antipova.Data.Models;

namespace OTUS_Homework_2_Antipova.Data.EntityConfigurations;

internal class GoodsStockEntityTypeConfiguration : IEntityTypeConfiguration<GoodsStockDbo>
{
    public void Configure(EntityTypeBuilder<GoodsStockDbo> builder)
    {
        builder.ToTable("goods_stock");

        builder.HasKey(x => x.Id);        

        builder.Property(x => x.Id)
            .ValueGeneratedOnAdd()
            .IsRequired();

        builder.Property(x => x.StockId)
            .IsRequired();

        builder.Property(x => x.GoodsId)
            .IsRequired();

        builder.Property(x => x.Count)
            .HasDefaultValue(0);

    }
}
