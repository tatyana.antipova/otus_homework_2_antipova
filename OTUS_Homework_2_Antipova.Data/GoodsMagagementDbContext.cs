﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OTUS_Homework_2_Antipova.Data.EntityConfigurations;
using OTUS_Homework_2_Antipova.Data.Models;


namespace OTUS_Homework_2_Antipova.Data;

public class GoodsMagagementDbContext : DbContext
{
    public GoodsMagagementDbContext() { }

    public GoodsMagagementDbContext(DbContextOptions<GoodsMagagementDbContext> options)
            : base(options)
    {
        Database.EnsureCreated();
    }

    public DbSet<GoodsDbo> Goods { get; set; }
    public DbSet<GoodsStockDbo> GoodsStock { get; set; }
    public DbSet<GoodsCategoryDbo> GoodsCategory { get; set; }
    public DbSet<StockDbo> Stock { get; set; }


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var builder = new ConfigurationBuilder().SetBasePath(Path.Combine(Directory.GetCurrentDirectory()))
                                                .AddJsonFile("appsettings.json", false)
                                                .Build();

        optionsBuilder.UseNpgsql(builder.GetConnectionString("DefaultConnection"));
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.UseIdentityColumns();

        modelBuilder.HasDefaultSchema("public");

        modelBuilder.ApplyConfiguration(new GoodsEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new GoodsCategoryEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new GoodsStockEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new StockEntityTypeConfiguration());


        modelBuilder.Seed();

        base.OnModelCreating(modelBuilder);
    }
}