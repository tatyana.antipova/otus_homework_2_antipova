﻿using Microsoft.EntityFrameworkCore;
using OTUS_Homework_2_Antipova.Data.Models;


namespace OTUS_Homework_2_Antipova.Data;

internal static class ModelBuilderExtensions
{
    public static void Seed(this ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<GoodsDbo>()
                    .HasData(
                        new GoodsDbo
                        {
                            Id = 1,
                            Title = "Набор зубных щёток",
                            Description = "Зубная щетка Hausland мягкая набор 2 шт., для взрослых, для чистки зубов и десен, универсальная, для гигиены и ухода за полостью рта.",
                            BasePrice = 150m,
                            CategoryId = 1,
                        },
                        new GoodsDbo
                        {
                            Id = 2,
                            Title = "Зубная нить \"Блеск межзубного пространства\"",
                            Description = "Одна сторона со щеткой (межзубный ершик) предназначена для бережной чистки межзубного пространства, другая - для удаления зубов.",
                            BasePrice = 250m,
                            CategoryId = 1,
                        },
                        new GoodsDbo
                        {
                            Id = 3,
                            Title = "Зубная нить \"Твоя цена\"",
                            Description = "Дешевое и сердитое решение для очистки зубов.",
                            BasePrice = 78m,
                            CategoryId = 1,
                        },
                        new GoodsDbo
                        {
                            Id = 4,
                            Title = "Контейнер для линз зеленый",
                            Description = "Южно-Корейский контейнер для линз простой и универсальный помощник для тех, кто регулярно пользуется контактными линзами. Цвет - зеленый.",
                            BasePrice = 180m
                        },
                        new GoodsDbo
                        {
                            Id = 5,
                            Title = "Контейнер для линз синий",
                            Description = "Южно-Корейский контейнер для линз простой и универсальный помощник для тех, кто регулярно пользуется контактными линзами. Цвет - синий.",
                            BasePrice = 180m
                        },
                        new GoodsDbo
                        {
                            Id = 6,
                            Title = "Говорящий умывальник \"Мойдодыр\"",
                            Description = "Чашеобразное приспособление, которое используется для мытья рук или небольших предметов.",
                            BasePrice = 2500m,
                            CategoryId = 2,
                        },
                        new GoodsDbo
                        {
                            Id = 7,
                            Title = "Мыло \"Земляничное\"",
                            Description = "Туалетное мыло РЕЦЕПТЫ ЧИСТОТЫ «Земляничное» очищает кожу, бережно ухаживая за ней. Средство непременно поднимет настроение благодаря аромату.",
                            BasePrice = 60m,
                            CategoryId = 2,
                        });

        modelBuilder.Entity<GoodsCategoryDbo>()
                    .HasData(
                        new GoodsCategoryDbo
                        {
                            Id = 1,
                            Name = "Гигиена рта",
                            Description = "Всё для домашнего ухода за зубами",
                        },
                        new GoodsCategoryDbo
                        {
                            Id = 2,
                            Name = "Гигиена тела",
                            Description = "Средства по уходу за кожей",
                        },
                        new GoodsCategoryDbo
                        {
                            Id = 3,
                            Name = "Детская гигиена",
                            Description = "Безопасные и гипоалергенные средства для детей.",
                        },
                        new GoodsCategoryDbo
                        {
                            Id = 4,
                            Name = "Специальная гигиена",
                            Description = "Средства которые помогают справиться с определенной проблемой",
                        },
                        new GoodsCategoryDbo
                        {
                            Id = 5,
                            Name = "Контейнеры и линзы",
                            Description = "Средства контактной коррекции зрения и аксессуары.",
                        });
       
        modelBuilder.Entity<StockDbo>()
            .HasData(
                new StockDbo
                {
                    Id = 1,
                    Title = "Склад Москва-1",
                    Capacity = 30000,
                    Address = "Москва, Ленина, 53",
                    AddressLatitude = 55.713656m,
                    AddressLongitude = 37.553878m,
                },
                new StockDbo
                {
                    Id = 2,
                    Title = "Склад Москва-2",
                    Capacity = 30000,
                    Address = "Москва, Ленина, 56",
                    AddressLatitude = 55.713656m,
                    AddressLongitude = 37.553898m,
                },
                new StockDbo
                {
                    Id = 3,
                    Title = "Склад Петербург-1",
                    Capacity = 50000,
                    Address = "Санкт-Петербург, Невский, 1",
                },
                new StockDbo
                {
                    Id = 4,
                    Title = "Склад Муром",
                    Capacity = 2000,
                    Address = "Муром, ПГТ Солнечный, Победы, 15",
                },
                new StockDbo
                {
                    Id = 5,
                    Title = "Склад Москва-3",
                    Capacity = 30000,
                    Address = "Москва, Ленина, 59",
                    AddressLatitude = 55.713698m,
                    AddressLongitude = 37.5538654m,
                });

        modelBuilder.Entity<GoodsStockDbo>()
            .HasData(
                new GoodsStockDbo
                {
                    Id = 1,
                    GoodsId = 1,
                    StockId = 2,
                    Count = 10
                },
                new GoodsStockDbo
                {
                    Id = 2,
                    GoodsId = 1,
                    StockId = 1,
                    Count = 500
                },
                new GoodsStockDbo
                {
                    Id = 3,
                    GoodsId = 2,
                    StockId = 5,
                    Count = 27
                },
                new GoodsStockDbo
                {
                    Id = 4,
                    GoodsId = 3,
                    StockId = 4,
                    Count = 140
                },
                new GoodsStockDbo
                {
                    Id = 5,
                    GoodsId = 5,
                    StockId = 5,
                    Count = 100
                });
    }
}