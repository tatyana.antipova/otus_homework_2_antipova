﻿namespace OTUS_Homework_2_Antipova.Data.Abstractions;

public interface IProvider <T>
{
    Task<IEnumerable<T>> GetAllAsync();

    Task AddAsync(T item);
}
