﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace OTUS_Homework_2_Antipova.Data.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "goods_gategory",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_goods_gategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "stock",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    Capacity = table.Column<int>(type: "integer", nullable: false),
                    Address = table.Column<string>(type: "text", nullable: true),
                    AddressLatitude = table.Column<decimal>(type: "numeric", nullable: true),
                    AddressLongitude = table.Column<decimal>(type: "numeric", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "goods",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    BasePrice = table.Column<decimal>(type: "numeric", nullable: false),
                    CategoryId = table.Column<int>(type: "integer", nullable: true),
                    GoodsStockId = table.Column<int>(type: "integer", nullable: true),
                    StockId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_goods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_goods_goods_gategory_CategoryId",
                        column: x => x.CategoryId,
                        principalSchema: "public",
                        principalTable: "goods_gategory",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "goods_stock",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    GoodsId = table.Column<int>(type: "integer", nullable: false),
                    StockId = table.Column<int>(type: "integer", nullable: false),
                    Count = table.Column<int>(type: "integer", nullable: false, defaultValue: 0)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_goods_stock", x => x.Id);
                    table.ForeignKey(
                        name: "FK_goods_stock_goods_GoodsId",
                        column: x => x.GoodsId,
                        principalSchema: "public",
                        principalTable: "goods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_goods_stock_stock_StockId",
                        column: x => x.StockId,
                        principalSchema: "public",
                        principalTable: "stock",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "GoodsDboStockDbo",
                schema: "public",
                columns: table => new
                {
                    GoodsId = table.Column<int>(type: "integer", nullable: false),
                    StockId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoodsDboStockDbo", x => new { x.GoodsId, x.StockId });
                    table.ForeignKey(
                        name: "FK_GoodsDboStockDbo_goods_GoodsId",
                        column: x => x.GoodsId,
                        principalSchema: "public",
                        principalTable: "goods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GoodsDboStockDbo_stock_StockId",
                        column: x => x.StockId,
                        principalSchema: "public",
                        principalTable: "stock",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "public",
                table: "goods",
                columns: new[] { "Id", "BasePrice", "CategoryId", "Description", "GoodsStockId", "StockId", "Title" },
                values: new object[,]
                {
                    { 4, 180m, null, "Южно-Корейский контейнер для линз простой и универсальный помощник для тех, кто регулярно пользуется контактными линзами. Цвет - зеленый.", null, null, "Контейнер для линз зеленый" },
                    { 5, 180m, null, "Южно-Корейский контейнер для линз простой и универсальный помощник для тех, кто регулярно пользуется контактными линзами. Цвет - синий.", null, null, "Контейнер для линз синий" }
                });

            migrationBuilder.InsertData(
                schema: "public",
                table: "goods_gategory",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Всё для домашнего ухода за зубами", "Гигиена рта" },
                    { 2, "Средства по уходу за кожей", "Гигиена тела" },
                    { 3, "Безопасные и гипоалергенные средства для детей.", "Детская гигиена" },
                    { 4, "Средства которые помогают справиться с определенной проблемой", "Специальная гигиена" },
                    { 5, "Средства контактной коррекции зрения и аксессуары.", "Контейнеры и линзы" }
                });

            migrationBuilder.InsertData(
                schema: "public",
                table: "stock",
                columns: new[] { "Id", "Address", "AddressLatitude", "AddressLongitude", "Capacity", "Title" },
                values: new object[,]
                {
                    { 1, "Москва, Ленина, 53", 55.713656m, 37.553878m, 30000, "Склад Москва-1" },
                    { 2, "Москва, Ленина, 56", 55.713656m, 37.553898m, 30000, "Склад Москва-2" },
                    { 3, "Санкт-Петербург, Невский, 1", null, null, 50000, "Склад Петербург-1" },
                    { 4, "Муром, ПГТ Солнечный, Победы, 15", null, null, 2000, "Склад Муром" },
                    { 5, "Москва, Ленина, 59", 55.713698m, 37.5538654m, 30000, "Склад Москва-3" }
                });

            migrationBuilder.InsertData(
                schema: "public",
                table: "goods",
                columns: new[] { "Id", "BasePrice", "CategoryId", "Description", "GoodsStockId", "StockId", "Title" },
                values: new object[,]
                {
                    { 1, 150m, 1, "Зубная щетка Hausland мягкая набор 2 шт., для взрослых, для чистки зубов и десен, универсальная, для гигиены и ухода за полостью рта.", null, null, "Набор зубных щёток" },
                    { 2, 250m, 1, "Одна сторона со щеткой (межзубный ершик) предназначена для бережной чистки межзубного пространства, другая - для удаления зубов.", null, null, "Зубная нить \"Блеск межзубного пространства\"" },
                    { 3, 78m, 1, "Дешевое и сердитое решение для очистки зубов.", null, null, "Зубная нить \"Твоя цена\"" },
                    { 6, 2500m, 2, "Чашеобразное приспособление, которое используется для мытья рук или небольших предметов.", null, null, "Говорящий умывальник \"Мойдодыр\"" },
                    { 7, 60m, 2, "Туалетное мыло РЕЦЕПТЫ ЧИСТОТЫ «Земляничное» очищает кожу, бережно ухаживая за ней. Средство непременно поднимет настроение благодаря аромату.", null, null, "Мыло \"Земляничное\"" }
                });

            migrationBuilder.InsertData(
                schema: "public",
                table: "goods_stock",
                columns: new[] { "Id", "Count", "GoodsId", "StockId" },
                values: new object[,]
                {
                    { 5, 100, 5, 5 },
                    { 1, 10, 1, 2 },
                    { 2, 500, 1, 1 },
                    { 3, 27, 2, 5 },
                    { 4, 140, 3, 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_goods_CategoryId",
                schema: "public",
                table: "goods",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_goods_stock_GoodsId",
                schema: "public",
                table: "goods_stock",
                column: "GoodsId");

            migrationBuilder.CreateIndex(
                name: "IX_goods_stock_StockId",
                schema: "public",
                table: "goods_stock",
                column: "StockId");

            migrationBuilder.CreateIndex(
                name: "IX_GoodsDboStockDbo_StockId",
                schema: "public",
                table: "GoodsDboStockDbo",
                column: "StockId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "goods_stock",
                schema: "public");

            migrationBuilder.DropTable(
                name: "GoodsDboStockDbo",
                schema: "public");

            migrationBuilder.DropTable(
                name: "goods",
                schema: "public");

            migrationBuilder.DropTable(
                name: "stock",
                schema: "public");

            migrationBuilder.DropTable(
                name: "goods_gategory",
                schema: "public");
        }
    }
}
