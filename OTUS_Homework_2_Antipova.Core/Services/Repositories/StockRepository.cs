﻿using OTUS_Homework_2_Antipova.Core.Abstractions;
using OTUS_Homework_2_Antipova.Core.Models;
using OTUS_Homework_2_Antipova.Data.Abstractions;
using OTUS_Homework_2_Antipova.Data.Models;
using OTUS_Homework_2_Antipova.Data.Providers;
using System.Text;

namespace OTUS_Homework_2_Antipova.Core.Services.Repositories;

public class StockRepository : IRepository<Stock>
{
    private readonly IProvider<StockDbo> _provider;

    public StockRepository(IProvider<StockDbo> provider)
    {
        _provider = provider;
    }

    public async Task<string> GetAllAsync()
    { 
        var result = await _provider.GetAllAsync();

        StringBuilder sb = new StringBuilder();
        
        foreach (var item in result)
        {
            sb.AppendLine(new Stock()
            {
                Id = item.Id,
                Title = item.Title,
                Capacity = item.Capacity,
                Address = item.Address,
                AddressLatitude = item.AddressLatitude,
                AddressLongitude = item.AddressLongitude
            }.ToString());
        }
        
        return sb.ToString();
    }

    public async Task<string> AddAsync(Stock item)
    {
        await _provider.AddAsync(new StockDbo()
        {
            Id = item.Id,
            Title = item.Title,
            Capacity = item.Capacity,
            Address = item.Address,
            AddressLatitude = item.AddressLatitude,
            AddressLongitude = item.AddressLongitude
        });
        return $"Элемент {nameof(StockDbo)} добавлен в таблицу.";
    }
}
