﻿using OTUS_Homework_2_Antipova.Core.Abstractions;
using OTUS_Homework_2_Antipova.Core.Models;
using OTUS_Homework_2_Antipova.Data.Abstractions;
using OTUS_Homework_2_Antipova.Data.Models;
using System.Text;

namespace OTUS_Homework_2_Antipova.Core.Services.Repositories;

public class GoodsRepository : IRepository<Goods>
{
    private readonly IProvider<GoodsDbo> _provider;
    public GoodsRepository(IProvider<GoodsDbo> provider)
    {
        _provider = provider;
    }

    public async Task<string> GetAllAsync()
    { 
        var result = await _provider.GetAllAsync();

        StringBuilder sb = new StringBuilder();
        
        foreach (var item in result)
        {
            sb.AppendLine(new Goods()
            {
                Id = item.Id,
                Title = item.Title,
                Description = item.Description,
                BasePrice = item.BasePrice,
                CategoryId = item.CategoryId,
            }.ToString());
        }
        
        return sb.ToString();
    }

    public async Task<string> AddAsync(Goods item)
    {
        await _provider.AddAsync(new GoodsDbo()
        {
            Id = item.Id,
            Title = item.Title,
            Description = item.Description,
            BasePrice = item.BasePrice,
            CategoryId = item.CategoryId,
        });
        return $"Элемент {nameof(GoodsDbo)} добавлен в таблицу.";
    }
}
