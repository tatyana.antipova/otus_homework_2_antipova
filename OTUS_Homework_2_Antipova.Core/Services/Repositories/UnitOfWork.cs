﻿using OTUS_Homework_2_Antipova.Core.Abstractions;
using OTUS_Homework_2_Antipova.Core.Models;

namespace OTUS_Homework_2_Antipova.Core.Services.Repositories;

public class UnitOfWork : IUnitOfWork
{
    private IRepository<Goods> _goodsRepository;
    private IRepository<Stock> _stockRepository;
    private IRepository<GoodsCategory> _goodsCategoryRepository;
    private IRepository<GoodsStock> _goodsStockRepository;

    public UnitOfWork(IRepository<Goods> goodsRepository,
        IRepository<Stock> stockRepository,
        IRepository<GoodsCategory> goodsCategoryRepository,
        IRepository<GoodsStock> goodsStockRepository)
    {
        _goodsRepository = goodsRepository;
        _stockRepository = stockRepository;
        _goodsCategoryRepository = goodsCategoryRepository;
        _goodsStockRepository = goodsStockRepository;
    }


    public IRepository<Goods> GetGoodsRepository() => _goodsRepository;
    public IRepository<Stock> GetStockRepository() => _stockRepository;
    public IRepository<GoodsCategory> GetGoodsCategoryRepository() => _goodsCategoryRepository;
    public IRepository<GoodsStock> GetGoodsStockRepository() => _goodsStockRepository;


}
