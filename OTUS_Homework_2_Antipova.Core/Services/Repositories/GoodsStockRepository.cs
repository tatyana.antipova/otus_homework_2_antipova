﻿using OTUS_Homework_2_Antipova.Core.Abstractions;
using OTUS_Homework_2_Antipova.Core.Models;
using OTUS_Homework_2_Antipova.Data.Abstractions;
using OTUS_Homework_2_Antipova.Data.Models;
using OTUS_Homework_2_Antipova.Data.Providers;
using System.Text;

namespace OTUS_Homework_2_Antipova.Core.Services.Repositories;

public class GoodsStockRepository : IRepository<GoodsStock>
{
    private readonly IProvider<GoodsStockDbo> _provider;
    public GoodsStockRepository(IProvider<GoodsStockDbo> provider)
    {
        _provider = provider;
    }

    public async Task<string> GetAllAsync()
    { 
        var result = await _provider.GetAllAsync();

        StringBuilder sb = new StringBuilder();
        
        foreach (var item in result)
        {
            sb.AppendLine(new GoodsStock()
            {
                Id = item.Id,
                GoodsId = item.GoodsId,
                StockId = item.StockId,
                Count = item.Count
            }.ToString());
        }
        
        return sb.ToString();
    }

    public async Task<string> AddAsync(GoodsStock item)
    {
        await _provider.AddAsync(new GoodsStockDbo()
        {
            Id = item.Id,
            GoodsId = item.GoodsId,
            StockId = item.StockId,
            Count = item.Count
        });
        return $"Элемент {nameof(GoodsStockDbo)} добавлен в таблицу.";
    }
}
