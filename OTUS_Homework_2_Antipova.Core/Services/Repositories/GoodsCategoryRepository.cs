﻿using OTUS_Homework_2_Antipova.Core.Abstractions;
using OTUS_Homework_2_Antipova.Core.Models;
using OTUS_Homework_2_Antipova.Data.Abstractions;
using OTUS_Homework_2_Antipova.Data.Models;
using System.Text;

namespace OTUS_Homework_2_Antipova.Core.Services.Repositories;

public class GoodsCategoryRepository : IRepository<GoodsCategory>
{
    private readonly IProvider<GoodsCategoryDbo> _provider;
    public GoodsCategoryRepository(IProvider<GoodsCategoryDbo> provider)
    {
        _provider = provider;
    }

    public async Task<string> GetAllAsync()
    { 
        var result = await _provider.GetAllAsync();

        StringBuilder sb = new StringBuilder();
        
        foreach (var item in result)
        {
            sb.AppendLine(new GoodsCategory()
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
            }.ToString());
        }
        
        return sb.ToString();
    }

    public async Task<string> AddAsync(GoodsCategory item)
    {
        await _provider.AddAsync(new GoodsCategoryDbo()
        {
            Id = item.Id,
            Name = item.Name,
            Description = item.Description,
        });
        return $"Элемент {nameof(GoodsCategoryDbo)} добавлен в таблицу.";
    }
}
