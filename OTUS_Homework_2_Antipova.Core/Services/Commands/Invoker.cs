﻿using OTUS_Homework_2_Antipova.Core.Abstractions;

namespace OTUS_Homework_2_Antipova.Core.Services.Commands;

public class Invoker
{
    private readonly IUnitOfWork _unitOfWork;

    public Invoker(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public ICommand GetCommand(string input)
    {
        if (string.IsNullOrWhiteSpace(input))
            return GetStartCommand();

        var parts = input.Split(' ');

        ICommand operation = parts[0] switch
        {
            StartCommand.Name => new StartCommand(),
            GetAllDataCommand.Name => new GetAllDataCommand(_unitOfWork),
            GetDataCommand.Name => new GetDataCommand(_unitOfWork, string.Join(' ', parts[1..])),
            SetDataCommand.Name => new SetDataCommand(_unitOfWork, string.Join(' ', parts[1..])),
            _ => throw new NotImplementedException()
        };

        return operation;    
    }

    public ICommand GetStartCommand()
    {
        return new StartCommand();
    }
}
