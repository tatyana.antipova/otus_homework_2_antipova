﻿using OTUS_Homework_2_Antipova.Core.Abstractions;

namespace OTUS_Homework_2_Antipova.Core.Services.Commands;

internal class StartCommand : ICommand
{
    public const string Name = "Start";

    public Task<string> Execute()
    {
        return Task.FromResult(@"Доступные команды:
    Start
    GetAllData
    GetData имя_таблицы
    SetData имя_таблицы

    Доступные таблицы:
    goods
    goods_category
    goods_stock
    stock
    ");
    }
}
