﻿using OTUS_Homework_2_Antipova.Core.Abstractions;
using OTUS_Homework_2_Antipova.Core.Models;

namespace OTUS_Homework_2_Antipova.Core.Services.Commands;

internal class SetDataCommand : ICommand
{
    public const string Name = "SetData";

    private readonly string _targetTable;
    private readonly IUnitOfWork _unitOfWork;

    public SetDataCommand(IUnitOfWork unitOfWork, string targetTable)
    {
        _targetTable = string.IsNullOrWhiteSpace(targetTable)
           ? throw new ArgumentNullException(targetTable)
           : targetTable;

        _unitOfWork = unitOfWork;
    }

    public async Task<string> Execute()
    {
        var result = _targetTable switch
        {
            "goods" => await _unitOfWork.GetGoodsRepository().AddAsync(GetUserData<Goods>()),
            "stock" => await _unitOfWork.GetStockRepository().AddAsync(GetUserData<Stock>()),
            "goods_category" => await _unitOfWork.GetGoodsCategoryRepository().AddAsync(GetUserData<GoodsCategory>()),
            "goods_stock" => await _unitOfWork.GetGoodsStockRepository().AddAsync(GetUserData<GoodsStock>()),
            _ => throw new ArgumentException($@"
Недопустимое название таблицы {_targetTable}. Доступные таблицы: 
    goods
    stock
    goods_category
    goods_stock
")
        };

        return result;
    }

    private T GetUserData<T>()
    {
        Console.WriteLine("Введите значения для новой записи:");
        T instance = (T)Activator.CreateInstance(typeof(T), true);
        
        foreach (var property in instance.GetType().GetProperties())
        {
            try
            {
                Type type = CastToUnderlyingType(property.PropertyType);

                Console.Write($"{property.Name} ({type.Name}): ");
                string val = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(val))
                {
                    property.SetValue(instance, Convert.ChangeType(val, type));
                }
                
            }
            catch (ArgumentException ex)
            { 
                Console.WriteLine (ex.Message);
            }
            catch (InvalidCastException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        return instance;
    }

    private Type CastToUnderlyingType(Type type)
    {
        return Nullable.GetUnderlyingType(type) != null 
            ? Nullable.GetUnderlyingType(type)!
            : type;
    }
}
