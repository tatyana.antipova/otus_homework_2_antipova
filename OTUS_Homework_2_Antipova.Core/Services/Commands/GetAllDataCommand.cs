﻿using OTUS_Homework_2_Antipova.Core.Abstractions;
using System.Text;

namespace OTUS_Homework_2_Antipova.Core.Services.Commands;

internal class GetAllDataCommand : ICommand
{
    public const string Name = "GetAllData";

    private readonly IUnitOfWork _unitOfWork;
    public GetAllDataCommand(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<string> Execute()
    {
        var goodsTask = await _unitOfWork.GetGoodsRepository().GetAllAsync();
        var stockTask = await _unitOfWork.GetStockRepository().GetAllAsync();
        var goodsCategoryTask = await _unitOfWork.GetGoodsCategoryRepository().GetAllAsync();
        var goodsStockTask = await _unitOfWork.GetGoodsStockRepository().GetAllAsync();

        StringBuilder sb = new StringBuilder();
        sb.AppendLine("goods:");
        sb.AppendLine(goodsTask);
        sb.AppendLine();

        sb.AppendLine("stock:");
        sb.AppendLine(stockTask);
        sb.AppendLine();

        sb.AppendLine("goods_category:");
        sb.AppendLine(goodsCategoryTask);
        sb.AppendLine();

        sb.AppendLine("goods_stock:");
        sb.AppendLine(goodsStockTask);
        sb.AppendLine();

        return sb.ToString();
    }
}
