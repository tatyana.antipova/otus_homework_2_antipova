﻿using OTUS_Homework_2_Antipova.Core.Abstractions;

namespace OTUS_Homework_2_Antipova.Core.Services.Commands;

public class GetDataCommand : ICommand
{
    public const string Name = "GetData";

    public readonly string _targetTable;
    
    private readonly IUnitOfWork _unitOfWork;

    public GetDataCommand(IUnitOfWork unitOfWork, string targetTable)
    {
        _targetTable = string.IsNullOrWhiteSpace(targetTable) 
            ? throw new ArgumentNullException(targetTable)
            : targetTable;
        
        _unitOfWork = unitOfWork;
    }

    public async Task<string> Execute()
    {
        var result = _targetTable switch
        {
            "goods" => await _unitOfWork.GetGoodsRepository().GetAllAsync(),
            "stock" => await _unitOfWork.GetStockRepository().GetAllAsync(),
            "goods_category" => await _unitOfWork.GetGoodsCategoryRepository().GetAllAsync(),
            "goods_stock" => await _unitOfWork.GetGoodsStockRepository().GetAllAsync(),
            _ => throw new ArgumentException($@"
Недопустимое название таблицы {_targetTable}. Доступные таблицы: 
    goods
    stock
    goods_category
    goods_stock
")
        };

        return result;
    }
}
