﻿namespace OTUS_Homework_2_Antipova.Core.Abstractions;

/// <summary>
/// Доступные к вызову команды.
/// </summary>
public interface ICommand
{
    Task<string> Execute();
}
