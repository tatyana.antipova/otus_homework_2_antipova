﻿using OTUS_Homework_2_Antipova.Core.Models;

namespace OTUS_Homework_2_Antipova.Core.Abstractions;

/// <summary>
/// Юнит работы с репозиториями.
/// </summary>
public interface IUnitOfWork
{
    IRepository<Goods> GetGoodsRepository();
    IRepository<Stock> GetStockRepository();
    IRepository<GoodsCategory> GetGoodsCategoryRepository();
    IRepository<GoodsStock> GetGoodsStockRepository();
}
