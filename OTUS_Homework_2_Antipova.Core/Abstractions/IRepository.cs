﻿namespace OTUS_Homework_2_Antipova.Core.Abstractions;

/// <summary>
/// Репозитории работы с данными.
/// </summary>
/// <typeparam name="T">Тип данных.</typeparam>
public interface IRepository<T>
{ 
    Task<string> GetAllAsync();
    Task<string> AddAsync(T item);
}
