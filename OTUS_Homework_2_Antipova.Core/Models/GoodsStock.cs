﻿namespace OTUS_Homework_2_Antipova.Core.Models;

public class GoodsStock
{
    public int Id { get; set; }
    public int GoodsId { get; set; }
    public int StockId { get; set; }
    public int Count { get; set; }

    public override string ToString()
    {
        return $@"
Id: {Id}
GoodsId: {GoodsId}
StockId: {StockId}
Count: {Count}
";
    }

}
