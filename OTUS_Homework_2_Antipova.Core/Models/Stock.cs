﻿namespace OTUS_Homework_2_Antipova.Core.Models;

public class Stock
{
    public int Id { get; set; }

    public string Title { get; set; }

    public int Capacity { get; set; }

    public string? Address { get; set; }

    public decimal? AddressLatitude { get; set; }

    public decimal? AddressLongitude { get; set; }

    public override string ToString()
    {
        return $@"
Id: {Id}
Title:  {Title}
Capacity: {Capacity}
Address:    {Address}
AddressLatitude:    {AddressLatitude}
AddressLongitude: {AddressLongitude}
";
    }

}
