﻿namespace OTUS_Homework_2_Antipova.Core.Models;

public class Goods
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public decimal BasePrice { get; set; }
    public int? CategoryId { get; set; }
    public override string ToString()
    {
        return $@"
Id: {Id}
Title:  {Title}
Description:    {Description}
BasePrice:  {BasePrice}
CategoryId: {CategoryId}
";
    }
}
