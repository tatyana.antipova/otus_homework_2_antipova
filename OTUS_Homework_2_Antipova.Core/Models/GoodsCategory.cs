﻿namespace OTUS_Homework_2_Antipova.Core.Models;

public class GoodsCategory
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public override string ToString()
    {
        return $@"
Id: {Id}
Name:   {Name}
Description:    {Description}
";
    }
}
